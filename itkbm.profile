<?php

/**
 * @file
 * Image Toolkit Benchmark install profile.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function itkbm_form_install_configure_form_alter(&$form, &$form_state, $form_id) {
  $form['site_information']['site_mail']['#default_value'] = 'admin@example.com';
  $form['site_information']['site_name']['#default_value'] = 'Image Toolkit Benchmark';
  $form['admin_account']['account']['mail']['#default_value'] = 'admin@example.com';
  $form['admin_account']['account']['name']['#default_value'] = 'Admin';

  if (isset($form['update_notifications'])) {
    $form['update_notifications']['update_status_module']['#default_value'] = array();
  }
}

/**
 * Implements hook_install_tasks().
 */
function itkbm_install_tasks(&$install_state) {
  $create_demo_content = !empty($install_state['parameters']['itkbm_demo_content_create']);

  return array(
    'itkbm_enable_image_toolkits' => array(
      'display_name' => st('Enable image toolkits'),
      'display' => TRUE,
      'type' => 'normal',
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'function' => 'itkbm_install_task_enable_image_toolkits',
    ),
    'itkbm_demo_content_form' => array(
      'display_name' => st('Demo content settings'),
      'display' => TRUE,
      'type' => 'form',
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'function' => 'itkbm_install_task_demo_content_form',
    ),
    'itkbm_demo_content_batch' => array(
      'display_name' => st('Create demo content'),
      'display' => $create_demo_content,
      'type' => 'batch',
      'run' => $create_demo_content ? INSTALL_TASK_RUN_IF_NOT_COMPLETED : INSTALL_TASK_SKIP,
      'function' => 'itkbm_install_task_demo_content_batch',
    ),
  );
}

/**
 * Implements callback_hook_install_tasks__function().
 */
function itkbm_install_task_enable_image_toolkits() {
  $modules = array('imagemagick', 'gmagick');

  if (extension_loaded('imagick')) {
    $modules[] = 'imagick';
  }

  module_enable($modules);
}

/**
 * Implements callback_hook_install_tasks__function().
 */
function itkbm_install_task_demo_content_form($form, &$form_state, &$install_state) {
  $form['demo_content_create'] = array(
    '#type' => 'checkboxes',
    '#required' => TRUE,
    '#title' => t('Create demo content'),
    '#default_value' => array(
      'rotate',
      'scale',
    ),
    '#options' => array(
      'rotate' => t('Rotate'),
      'scale' => t('Scale'),
    ),
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Send'),
    ),
  );

  return $form;
}

/**
 * Form API #submit callback.
 */
function itkbm_install_task_demo_content_form_submit($form, &$form_state) {
  $demo_content_types = array_keys($form_state['values']['demo_content_create'], TRUE);
  if (!$demo_content_types) {
    return;
  }

  $install_state =& $form_state['build_info']['args'][0];
  $install_state['parameters']['itkbm_demo_content_create'] = $demo_content_types;

  module_enable(array('itkbm_styles'));
  features_revert_module('itkbm_styles');
}

/**
 * Implements callback_hook_install_tasks__function().
 */
function itkbm_install_task_demo_content_batch(&$install_state) {
  $batch = array(
    'operations' => array(),
    'title' => t('Create demo contents'),
    'init_message' => t('Initializing.'),
    'progress_message' => t('Completed @current of @total.'),
    'error_message' => t('An error has occurred.'),
    'finished' => 'itkbm_install_task_demo_content_batch_finished',
  );

  $resolutions = array(
    array('width' => '720', 'height' => '480'),
    array('width' => '720', 'height' => '576'),
    array('width' => '1280', 'height' => '720'),
    //array('width' => '1440', 'height' => '1080'),
    //array('width' => '1920', 'height' => '1080'),
    //array('width' => '1998', 'height' => '1080'),
    //array('width' => '2048', 'height' => '1080'),
    //array('width' => '3840', 'height' => '2160'),
    //array('width' => '4096', 'height' => '2160'),
    //array('width' => '7680', 'height' => '4320'),
    //array('width' => '15360', 'height' => '8640'),
  );

  $limit = 10;
  foreach (array('jpg', 'png') as $extension) {
    foreach ($resolutions as $resolution) {
      $batch['operations'][] = array(
        'itkbm_install_task_demo_content_batch_process_images',
        array(
          $extension,
          $resolution['width'],
          $resolution['height'],
          $limit
        )
      );
    }
  }

  $image_styles = image_styles();
  $depots = array(
    'rotate' => array('title' => t('Rotate')),
    'scale' => array('title' => t('Scale')),
  );

  $toolkits = array_keys(image_get_available_toolkits());
  foreach ($depots as $prefix => $info) {
    if (!in_array($prefix, $install_state['parameters']['itkbm_demo_content_create'])) {
      continue;
    }

    $node = new stdClass();
    $node->type = 'itkbm_depot';
    $node->title = $info['title'];
    $node->uid = 1;
    $node->created = REQUEST_TIME;
    $node->validated = TRUE;
    $node->status = 1;
    $node->promote = 1;
    node_save($node);

    $selected_image_styles = array();
    foreach (array_keys($image_styles) as $image_style) {
      if (strpos($image_style, "{$prefix}-") === 0) {
        $selected_image_styles[] = $image_style;
      }
    }

    $batch['operations'][] = array(
      'itkbm_install_task_demo_content_batch_process_depot',
      array(
        $node->nid,
        $selected_image_styles,
      )
    );

    foreach ($toolkits as $toolkit) {
      foreach ($selected_image_styles as $image_style) {
        $batch['operations'][] = array(
          'itkbm_meter_derive_batch_process',
          array($node->nid, $toolkit, $image_style)
        );
      }
    }
  }

  return $batch;
}

/**
 * Batch API process callback.
 *
 * Create dummy images.
 */
function itkbm_install_task_demo_content_batch_process_images($extension, $width, $height, $limit, &$context) {
  if (!isset($context['sandbox']['initialized'])) {
    $context['sandbox']['initialized'] = TRUE;
    $context['sandbox']['images_current'] = 0;

    module_enable(array('devel_generate'));
  }

  module_load_include('inc', 'devel_generate', 'image.devel_generate');

  $resolution = "{$width}x{$height}";
  $dst_dir = "public://itkbm-generated";

  file_prepare_directory($dst_dir, FILE_CREATE_DIRECTORY);

  $round_limit = 100;
  $round_current = 0;
  while ($round_current < $round_limit && $context['sandbox']['images_current'] < $limit) {
    $round_current++;
    $context['sandbox']['images_current']++;

    $file_name = "{$resolution}-{$context['sandbox']['images_current']}.$extension";

    $image = new stdClass();
    $image->uri = devel_generate_image($extension, $resolution, $resolution);
    $image->uid = 1;
    $image->filename = $file_name;
    $image->mime = file_get_mimetype($image->uri);
    $image->status = FILE_STATUS_PERMANENT;
    $image->timestamp = REQUEST_TIME;

    $dst_uri = "$dst_dir/$file_name";
    $file = file_copy($image, $dst_uri);
    file_save($file)->fid;
  }

  $context['finished'] = $context['sandbox']['images_current'] / $limit;
  $context['message'] = format_plural(
    $limit,
    'Generate @count dummy image with @width×@height resolution.',
    'Generate @count dummy images with @width×@height resolution.',
    array(
      '@width' => $width,
      '@height' => $height,
    )
  );

  if ($context['finished'] == 1) {
    module_disable(array('devel_generate'));
  }
}

/**
 * Batch API callback.
 */
function itkbm_install_task_demo_content_batch_process_depot($depot_id, $image_styles, &$context) {
  if (!isset($context['sandbox']['initialized'])) {
    $context['sandbox']['initialized'] = TRUE;

    $context['sandbox']['last_image_id'] = 0;
    $context['sandbox']['images_current'] = 0;
    $context['sandbox']['images_total'] = db_select('file_managed', 'fm')
      ->fields('fm', array('fid'))
      ->condition('fm.type', 'image')
      ->countQuery()
      ->execute()
      ->fetchColumn();

    if (!$context['sandbox']['images_total']) {
      $context['finished'] = 1;

      return;
    }
  }

  $depot = node_load($depot_id);
  $toolkits = itkbm_meter_image_toolkit_options();
  $repeat_target = 1;

  $image_ids = db_select('file_managed', 'fm')
    ->fields('fm', array('fid'))
    ->condition('fm.type', 'image')
    ->condition('fm.fid', $context['sandbox']['last_image_id'], '>')
    ->orderBy('fm.fid')
    ->range(0, 100)
    ->execute()
    ->fetchCol();

  itkbm_meter_depot_images_add(
    $depot->nid,
    $image_ids,
    array_keys($toolkits),
    $image_styles,
    $repeat_target
  );

  $context['sandbox']['last_image_id'] = end($image_ids);
  $context['sandbox']['images_current'] += count($image_ids);

  $context['finished'] = $context['sandbox']['images_current'] / $context['sandbox']['images_total'];
  $context['message'] = t('Add images to %depot depot', array('%depot' => $depot->title));
}

/**
 * Batch API callback.
 */
function itkbm_install_task_demo_content_batch_finished($success, $results, $operations, $elapsed) {
  if (module_exists('devel_generate')) {
    module_disable(array('devel_generate'));
  }

  drupal_set_message(t('@todo'));
}
