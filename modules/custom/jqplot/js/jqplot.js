/**
 * @file
 * Documentation missing.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.jqplot = {
    attach: function (context, settings) {
      var chartId;
      if (!settings.hasOwnProperty('jqplot') || !settings.jqplot.hasOwnProperty('chart')) {
        return;
      }

      for (chartId in settings.jqplot.chart) {
        if (settings.jqplot.chart.hasOwnProperty(chartId)) {
          $('#' + chartId).once('jqplot', Drupal.jqplot.initializeChart);
        }
      }
    }
  };

  Drupal.jqplot = Drupal.jqplot || {};
  Drupal.jqplot.chart = Drupal.jqplot.chart || {};

  Drupal.jqplot.rendererIsInitialized = false;
  Drupal.jqplot.renderer = {};

  Drupal.jqplot.rendererNames = [
    'Bar',
    'CategoryAxis'
  ];

  Drupal.jqplot.initializeRenderer = function () {
    if (Drupal.jqplot.rendererIsInitialized) {
      return;
    }

    Drupal.jqplot.rendererIsInitialized = true;

    var i, name;

    for (i = 0; i < Drupal.jqplot.rendererNames.length; i++) {
      name = Drupal.jqplot.rendererNames[i];
      if ($.jqplot.hasOwnProperty(name + 'Renderer')) {
        Drupal.jqplot.renderer[name] = $.jqplot[name + 'Renderer'];
      }
    }

  };

  Drupal.jqplot.initializeChart = function () {
    var
      chartId = this.id,
      chartMeta = Drupal.settings.jqplot.chart[chartId],
      method = 'initializeChart' + chartMeta.type;

    chartMeta.id = chartId;
    if (typeof(Drupal.jqplot[method]) == 'function') {
      Drupal.jqplot[method](chartMeta);
    }

    $.jqplot(
      chartMeta.id,
      chartMeta.data,
      chartMeta.options
    );
  };

  Drupal.jqplot.initializeChartBar = function (chartMeta) {
    chartMeta.options.seriesDefaults = chartMeta.options.seriesDefaults || {};
    chartMeta.options.seriesDefaults.renderer = chartMeta.options.seriesDefaults.renderer || null;
    chartMeta.options.seriesDefaults.renderer = Drupal.jqplot.getRenderer(
      chartMeta.options.seriesDefaults.renderer,
      $.jqplot.BarRenderer
    );

    chartMeta.options.axes = chartMeta.options.axes || {};
    chartMeta.options.axes.yaxis = chartMeta.options.axes.yaxis || {};
    chartMeta.options.axes.yaxis.renderer = chartMeta.options.axes.yaxis.renderer || null;
    chartMeta.options.axes.yaxis.renderer = Drupal.jqplot.getRenderer(
      chartMeta.options.axes.yaxis.renderer,
      $.jqplot.CategoryAxisRenderer
    );
  };
  
  Drupal.jqplot.getRenderer = function(renderer, def) {
    if (typeof renderer === 'function') {
      return renderer;
    }

    if (typeof renderer === 'stringValue' && Drupal.jqplot.renderer.hasOwnProperty(renderer)) {
      return Drupal.jqplot.renderer[renderer];
    }

    return def;
  };

}(jQuery));
