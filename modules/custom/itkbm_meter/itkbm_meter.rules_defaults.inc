<?php
/**
 * @file
 * itkbm_meter.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function itkbm_meter_default_rules_configuration() {
  $items = array();
  $export = <<< 'JSON'
{ "itkbm_images_add" : {
      "LABEL" : "Add images to an existing ITKBM set",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "TAGS" : [ "itkbm", "vbo" ],
      "REQUIRES" : [ "itkbm_meter" ],
      "USES VARIABLES" : {
        "images" : { "label" : "Images", "type" : "list\u003Cfile\u003E" },
        "depot" : { "label" : "ITKBM Depot", "type" : "node", "restriction" : "input" },
        "toolkits" : {
          "label" : "Toolkits",
          "type" : "list\u003Ctext\u003E",
          "optional" : true,
          "options list" : "itkbm_meter_image_toolkit_options",
          "restriction" : "input"
        },
        "image_styles" : {
          "label" : "Image styles",
          "type" : "list\u003Ctext\u003E",
          "optional" : true,
          "options list" : "itkbm_meter_image_style_options",
          "restriction" : "input"
        }
      },
      "ACTION SET" : [
        { "itkbm_meter_itkbm_images_add" : {
            "images" : [ "images" ],
            "depot" : [ "depot" ],
            "toolkits" : [ "toolkits" ],
            "image_styles" : [ "image_styles" ]
          }
        }
      ]
    }
  }
JSON;
  $items['itkbm_images_add'] = entity_import('rules_config', $export);

  $export = <<< 'JSON'
{ "itkbm_images_remove" : {
      "LABEL" : "ITKBM - Images - Remove",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "TAGS" : [ "itkbm", "vbo" ],
      "REQUIRES" : [ "itkbm_meter" ],
      "USES VARIABLES" : {
        "images" : { "label" : "Images", "type" : "list\u003Cfile\u003E" },
        "depot" : { "label" : "ITKBM Depot", "type" : "node" }
      },
      "ACTION SET" : [
        { "itkbm_meter_itkbm_images_remove" : { "images" : [ "images" ], "depot" : [ "depot" ] } }
      ]
    }
  }
JSON;
  $items['itkbm_images_remove'] = entity_import('rules_config', $export);

  return $items;
}
