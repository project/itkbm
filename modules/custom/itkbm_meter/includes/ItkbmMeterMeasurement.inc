<?php
/**
 * @file
 * Home of ItkbmMeterMeasurement.
 */

/**
 * Class ItkbmMeterMeasurement.
 */
class ItkbmMeterMeasurement {

  /**
   * @var int
   */
  public $id = 0;

  /**
   * Date.
   *
   * @var string
   */
  public $changed = '';

  /**
   * Node ID.
   *
   * @var int
   */
  public $depot = 0;

  /**
   * @var string
   */
  public $imageStyle = '';

  /**
   * @var string
   */
  public $toolkit = '';

  /**
   * File ID.
   *
   * @var int
   */
  public $image = 0;

  /**
   * @var int
   */
  public $repeatTarget = 0;

  /**
   * @var int
   */
  public $repeatCurrent = 0;

  /**
   * @var int
   */
  public $fileSize = 0;

  /**
   * @var int
   */
  public $memoryBefore = 0;

  /**
   * @var int
   */
  public $memoryAfter = 0;

  /**
   * @var int
   */
  public $memoryUsage = 0;

  /**
   * @var int
   */
  public $memoryRatio = 0;

  /**
   * @var int
   */
  public $duration = 0;

  /**
   * @var int
   */
  public $durationRatio = 0;

  /**
   * @param array $values
   */
  public function __construct(array $values = array()) {
    foreach ($values as $key => $value) {
      $this->$key = $value;
    }
  }

}
