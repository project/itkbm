<?php

/**
 * @file
 * Plugin definition and callbacks for a ctools:content_types plugin.
 *
 * @ingroup CToolsPlugin CToolsPluginContentTypes
 */

$plugin = array(
  'category' => t('ITKBM Report'),
  'title' => t('ITKBM: Depot statistics'),
  'description' => t('Long description.'),

  'all contexts' => TRUE,
  'required context' => array(
    new ctools_context_required(t('Depot'), array('entity:node')),
  ),

  'defaults' => array(
    'properties' => array(
      'memoryUsageAVG' => FALSE,
      'memoryRatioAVG' => FALSE,
      'durationAVG' => TRUE,
      'durationRatioAVG' => FALSE,
    ),
  ),
  'no title override' => FALSE,
  'edit form'          => 'itkbm_meter_ctools_content_types_depot_statistics_edit_form',
  'edit form validate' => 'itkbm_meter_ctools_content_types_depot_statistics_edit_form_validate',
  'edit form submit'   => 'itkbm_meter_ctools_content_types_depot_statistics_edit_form_submit',

  'admin info' => 'itkbm_meter_ctools_content_types_depot_statistics_admin_info',

  'render callback' => 'itkbm_meter_ctools_content_types_depot_statistics_render',
);

/**
 * Edit form callback.
 */
function itkbm_meter_ctools_content_types_depot_statistics_edit_form($form, &$form_state) {
  $defaults = array();
  if (isset($form_state['subtype']['defaults'])) {
    $defaults = $form_state['subtype']['defaults'];
  }
  elseif (isset($form_state['plugin']['defaults'])) {
    $defaults = $form_state['plugin']['defaults'];
  }

  $conf = $form_state['conf'] + $defaults;

  $form['properties'] = array(
    '#type' => 'checkboxes',
    '#required' => TRUE,
    '#title' => t('Properties'),
    '#default_value' => $conf['properties'],
    '#options' => array(
      'fileSizeAVG' => t('Average file size'),
      'memoryUsageAVG' => t('Average memory usage'),
      'memoryRatioAVG' => t('Average memory ratio'),
      'durationAVG' => t('Average duration'),
      'durationRatioAVG' => t('Average duration ratio'),
    ),
  );

  return $form;
}

/**
 * Edit form submit callback.
 */
function itkbm_meter_ctools_content_types_depot_statistics_edit_form_submit($form, &$form_state) {
  $defaults = array();
  if (isset($form_state['subtype']['defaults'])) {
    $defaults = $form_state['subtype']['defaults'];
  }
  elseif (isset($form_state['plugin']['defaults'])) {
    $defaults = $form_state['plugin']['defaults'];
  }

  foreach (array_keys($defaults) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Admin info callback.
 *
 * @param string $subtype
 *   Identifier of subtype.
 * @param array $conf
 *   Pane configuration. Array keys are depends on the _edit_form.
 *   - override_title
 *   - override_title_text
 * @param array $context
 *   An array of context objects available for use. These may be placeholders.
 *
 * @return object
 *   Administrative title and description of the $subtype pane.
 *   The keys are:
 *   - title: string
 *   - content: string|array
 */
function itkbm_meter_ctools_content_types_depot_statistics_admin_info($subtype, $conf, $context) {
  $return = new stdClass();
  $return->title = 'Foo';
  $return->content = "This is the admin info of itkbm_meter:depot_statistics:$subtype";

  return $return;
}

/**
 * Render callback.
 *
 * "CTools:Content types" plugin render callback for "ITKBM:Depot statistics".
 *
 * @param string $subtype
 *   Subtype identifier.
 * @param array  $conf
 *   Configuration of the $subtype instance.
 * @param array  $args
 *   Documentation missing.
 * @param array  $pane_context
 *   Documentation missing.
 * @param array  $incoming_content
 *   Documentation missing.
 *
 * @return object
 *   The content.
 */
function itkbm_meter_ctools_content_types_depot_statistics_render($subtype, $conf, $args, $pane_context, $incoming_content) {
  /** @var ctools_context $depot_context */
  $depot_context = (isset($pane_context[$conf['context'][0]]) ? $pane_context[$conf['context'][0]] : NULL);
  $depot_id = $depot_context->data->nid;

  $chart = itkbm_meter_depot_statistics_chart($depot_id, $conf['properties']);
  if (!$chart) {
    return NULL;
  }

  $block = new stdClass();
  $block->module = 'itkbm_meter';
  $block->delta = $subtype;
  $block->title = '@todo';
  $block->content = array('chart' => $chart);

  return $block;
}

