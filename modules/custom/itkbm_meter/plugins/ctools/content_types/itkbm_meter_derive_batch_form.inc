<?php

/**
 * @file
 * Plugin definition and callbacks for a ctools:content_types plugin.
 *
 * @ingroup CToolsPlugin CToolsPluginContentTypes
 */

$plugin = array(
  'category' => t('ITKBM'),
  'title' => t('ITKBM: Derive batch form'),
  'description' => t('Long description.'),

  'all contexts' => TRUE,
  'required context' => array(
    new ctools_context_required(t('Depot'), array('entity:node')),
  ),

  'defaults' => array(),
  'no title override' => FALSE,
  // Delete the 'add_form' if no differences between *_add_form and *_edit_form.
  //'add form'           => 'itkbm_meter_ctools_content_types_derive_batch_form_add_form',
  //'add form validate'  => 'itkbm_meter_ctools_content_types_derive_batch_form_add_form_validate',
  //'add form submit'    => 'itkbm_meter_ctools_content_types_derive_batch_form_add_form_submit',
  'edit form'          => 'itkbm_meter_ctools_content_types_derive_batch_form_edit_form',
  'edit form validate' => 'itkbm_meter_ctools_content_types_derive_batch_form_edit_form_validate',
  'edit form submit'   => 'itkbm_meter_ctools_content_types_derive_batch_form_edit_form_submit',

  'admin info' => 'itkbm_meter_ctools_content_types_derive_batch_form_admin_info',

  'render callback' => 'itkbm_meter_ctools_content_types_derive_batch_form_render',
);

/**
 * Collect the subtypes.
 */
/*
function itkbm_meter_ctools_content_types_derive_batch_form_content_types($plugin) {
  $collection = array(
    'one' => (object) array(
      'id' => 'one',
      'title' => t('One'),
      'description' => t('This is the description - One'),
    ),
    'two' => (object) array(
      'id' => 'two',
      'title' => t('Two'),
      'description' => t('This is the description - Two'),
    ),
  );

  $return = array();
  foreach ($collection as $item) {
    $return[$item->id] = array(
      'subtype' => $item->id,
      'category' => $plugin['category'],
      'title' => $item->title,
      'description' => filter_xss_admin($item->description),
      // https://drupal.org/node/2036415
      'check editable' => 'itkbm_meter_ctools_content_types_derive_batch_form_check_editable',
    );
  }

  return $return;
}
*/

/**
 * Check the content type configuration is editable or not.
 *
 * @param array $plugin
 *   The main plugin definition.
 * @param array $subtype
 *   The subtype plugin definition.
 * @param array $conf
 *   Content type configuration. Array keys are depends on the _edit_form.
 *   - override_title
 *   - override_title_text
 *
 * @return bool
 *   Returns TRUE if the content configuration is editable, FALSE otherwise.
 */
/*
function itkbm_meter_ctools_content_types_derive_batch_form_check_editable($plugin, $subtype, $conf) {
  return TRUE;
}
*/

/**
 * Add form callback.
 */
 /*
function itkbm_meter_ctools_content_types_derive_batch_form_add_form($form, &$form_state) {
  return $form;
}
*/

/**
 * Add form validate callback.
 */
 /*
function itkbm_meter_ctools_content_types_derive_batch_form_add_form_validate($form, &$form_state) {
  itkbm_meter_ctools_content_types_derive_batch_form_edit_form_validate($form, $form_state);
}
*/

/**
 * Add form submit callback.
 */
 /*
function itkbm_meter_ctools_content_types_derive_batch_form_add_form_submit($form, &$form_state) {
  itkbm_meter_ctools_content_types_derive_batch_form_edit_form_submit($form, $form_state);
}
*/

/**
 * Edit form callback.
 */
function itkbm_meter_ctools_content_types_derive_batch_form_edit_form($form, &$form_state) {
  $defaults = array();
  if (isset($form_state['subtype']['defaults'])) {
    $defaults = $form_state['subtype']['defaults'];
  }
  elseif (isset($form_state['plugin']['defaults'])) {
    $defaults = $form_state['plugin']['defaults'];
  }

  $conf = $form_state['conf'] + $defaults;

  return $form;
}

/**
 * Edit form validate callback.
 */
/*
function itkbm_meter_ctools_content_types_derive_batch_form_edit_form_validate($form, &$form_state) {
}
*/

/**
 * Edit form submit callback.
 */
function itkbm_meter_ctools_content_types_derive_batch_form_edit_form_submit($form, &$form_state) {
  $defaults = array();
  if (isset($form_state['subtype']['defaults'])) {
    $defaults = $form_state['subtype']['defaults'];
  }
  elseif (isset($form_state['plugin']['defaults'])) {
    $defaults = $form_state['plugin']['defaults'];
  }

  foreach (array_keys($defaults) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Admin info callback.
 *
 * @param string $subtype
 *   Identifier of subtype.
 * @param array $conf
 *   Pane configuration. Array keys are depends on the _edit_form.
 *   - override_title
 *   - override_title_text
 * @param ctools_context[] $context
 *   An array of context objects available for use. These may be placeholders.
 *
 * @return object
 *   Administrative title and description of the $subtype pane.
 *   The keys are:
 *   - title: string
 *   - content: string|array
 */
function itkbm_meter_ctools_content_types_derive_batch_form_admin_info($subtype, $conf, $context) {
  $depot_context_name = $conf['context'][0];
  $depot_context = $context[$depot_context_name];

  $return = new stdClass();
  $return->title = t('Manage');
  $return->content = t(
    'Manage the measurements of the %depot depot.',
    array(
      '%depot' => $depot_context->get_identifier(),
    )
  );

  return $return;
}

/**
 * Render callback.
 *
 * "CTools:Content types" plugin render callback for "ITKBM:Derive batch form".
 *
 * @param string $subtype
 *   Subtype identifier.
 * @param array  $conf
 *   Configuration of the $subtype instance.
 * @param array  $args
 *   Documentation missing.
 * @param array  $pane_context
 *   Documentation missing.
 * @param array  $incoming_content
 *   Documentation missing.
 *
 * @return object
 *   The content.
 */
function itkbm_meter_ctools_content_types_derive_batch_form_render($subtype, $conf, $args, $pane_context, $incoming_content) {
  /** @var ctools_context $depot_context */
  $depot_context = (isset($pane_context[$conf['context'][0]]) ? $pane_context[$conf['context'][0]] : NULL);

  $block = new stdClass();
  $block->module = 'itkbm_meter';
  $block->delta = 'derive_batch_form';
  $block->title = t('Process the remaining images in %label depot.', array(
    '%label' => $depot_context->data->title,
  ));
  $block->content = array(
    'form' => drupal_get_form('itkbm_meter_derive_batch_form', $depot_context->data),
  );

  return $block;
}
