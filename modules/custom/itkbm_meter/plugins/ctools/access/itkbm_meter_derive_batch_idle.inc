<?php

/**
 * @file
 * Plugin definition and callbacks for a ctools:access plugin.
 *
 * @ingroup CToolsPlugin CToolsPluginAccess
 */

$plugin = array(
  'title' => t('ITKBM: Derive batch is idle'),
  'required context' => array(),
  'defaults' => array(),
  'summary'  => 'itkbm_meter_ctools_access_derive_batch_idle_summary',
  'callback' => 'itkbm_meter_ctools_access_derive_batch_idle_check',
);

/**
 * Settings summary callback for CTools access plugin: ITKBM: Derive batch is idle.
 *
 * @return string
 *   Human readable summary of the configuration.
 */
function itkbm_meter_ctools_access_derive_batch_idle_summary() {
  $progress = variable_get('itkbm_meter_derive_progress', array('status' => 'idle'));

  return ($progress['status'] === 'idle' ?
    t('ITKBM: Derive batch is idle') : t('ITKBM: Derive batch is not idle')
  );
}

/**
 * Find whether the access is granted.
 *
 * "CTools:Access" plugin callback for "ITKBM:Derive batch is idle".
 *
 * @return bool
 *   Returns TRUE if access is granted, FALSE otherwise.
 */
function itkbm_meter_ctools_access_derive_batch_idle_check() {
  $progress = variable_get('itkbm_meter_derive_progress', array('status' => 'idle'));

  return $progress['status'] === 'idle';
}
