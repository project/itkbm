<?php

/**
 * @file
 * Plugin definition and callbacks for a ctools:access plugin.
 *
 * @ingroup CToolsPlugin CToolsPluginAccess
 */

$plugin = array(
  'title' => t('ITKBM: Depot is empty'),
  'description' => t('Check a certain depot does not contain any image.'),

  'required context' => array(
    new ctools_context_required(t('Depot'), array('entity:node')),
  ),

  'defaults' => array(),
  //'settings form'          => 'itkbm_meter_ctools_access_depot_is_empty_settings_form',
  //'settings form validate' => 'itkbm_meter_ctools_access_depot_is_empty_settings_form_validate',
  //'settings form submit'   => 'itkbm_meter_ctools_access_depot_is_empty_settings_form_submit',
  //'restrictions'           => 'itkbm_meter_ctools_access_depot_is_empty_restrictions',
  'summary'                => 'itkbm_meter_ctools_access_depot_is_empty_summary',

  //'get child'              => 'itkbm_meter_ctools_access_depot_is_empty_get_child',
  //'get children'           => 'itkbm_meter_ctools_access_depot_is_empty_get_children',

  'callback'               => 'itkbm_meter_ctools_access_depot_is_empty_check',
);

/**
 * Settings summary callback for CTools access plugin: ITKBM: Depot has an image.
 *
 * @param array|null $settings
 *   Access configuration. Array keys are depends on the _settings_form.
 * @param ctools_context[] $contexts
 *   An array of context objects available for use. These may be placeholders.
 *
 * @return string
 *   Human readable summary of the configuration.
 */
function itkbm_meter_ctools_access_depot_is_empty_summary($settings, $contexts) {
  if (isset($contexts[0])) {
    $summary = t('%label depot is empty.', array(
      '%label' => $contexts[0]->get_identifier(),
    ));
  }
  else {
    $summary = t('@todo');
  }

  return $summary;
}

/**
 * Find whether the access is granted.
 *
 * "CTools:Access" plugin callback for "ITKBM:Depot has an image".
 *
 * @param array|null $settings
 *   Access configuration. Array keys are depends on the _settings_form.
 * @param array $contexts
 *   Array of available contexts.
 * @param array $plugin
 *   Plugin definition.
 *
 * @return bool
 *   Returns TRUE if access is granted, FALSE otherwise.
 */
function itkbm_meter_ctools_access_depot_is_empty_check($settings, $contexts, $plugin) {
  $depot = (isset($contexts[0]) && !$contexts[0]->empty ? $contexts[0]->data : NULL);

  return ($depot && itkbm_meter_depot_image_count($depot->nid) === 0);
}

