<?php

/**
 * @file
 * Plugin definition and callbacks for a ctools:access plugin.
 *
 * @ingroup CToolsPlugin CToolsPluginAccess
 */

$plugin = array(
  'title' => t('ITKBM: Depot complete'),
  'required context' => array(
    new ctools_context_required(t('Depot'), array('entity:node')),
  ),
  'defaults' => array(),
  'summary'                => 'itkbm_meter_ctools_access_depot_complete_summary',
  'callback'               => 'itkbm_meter_ctools_access_depot_complete_check',
);

/**
 * @param array|null $settings
 * @param ctools_context[] $contexts
 * @param array $plugin
 *
 * @return string
 */
function itkbm_meter_ctools_access_depot_complete_summary($settings, $contexts, $plugin) {
  if (isset($contexts[0])) {
    $summary = t('%label has no any measurement to process.', array(
      '%label' => $contexts[0]->get_identifier(),
    ));
  }
  else {
    $summary = t('@todo');
  }

  return $summary;
}

/**
 * Find whether the access is granted.
 *
 * "CTools:Access" plugin callback for "ITKBM:Depot complete".
 *
 * @param array|null $settings
 *   Access configuration. Array keys are depends on the _settings_form.
 * @param array $contexts
 *   Array of available contexts.
 * @param array $plugin
 *   Plugin definition.
 *
 * @return bool
 *   Returns TRUE if access is granted, FALSE otherwise.
 */
function itkbm_meter_ctools_access_depot_complete_check($settings, $contexts, $plugin) {
  $depot = (isset($contexts[0]) && !$contexts[0]->empty ? $contexts[0]->data : NULL);

  return ($depot && itkbm_meter_depot_num_of_remaining($depot->nid) == 0);
}
