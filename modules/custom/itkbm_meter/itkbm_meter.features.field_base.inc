<?php
/**
 * @file
 * itkbm_meter.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function itkbm_meter_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'itkbm_description'
  $field_bases['itkbm_description'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'itkbm_description',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'itkbm_exif_orientation'
  $field_bases['itkbm_exif_orientation'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'itkbm_exif_orientation',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'Unknown',
        1 => 'Normal',
        2 => 'Mirror Vertically',
        3 => 'Rotate 180',
        4 => 'Mirror Horizontally',
        5 => 'Rotate 90, Mirror Vertically',
        6 => 'Rotate 90',
        7 => 'Rotate 270, Mirror Vertically',
        8 => 'Rotate 270',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_integer',
  );

  return $field_bases;
}
