<?php

/**
 * @file
 * Rules integration of ITKBM - Meter module.
 */

/**
 * Implements hook_rules_action_info().
 */
function itkbm_meter_rules_action_info() {
  $info = array();

  $info['itkbm_meter_itkbm_images_add'] = array(
    'group' => t('Image Toolkit Benchmark'),
    'label' => t('Add images to an existsing ITKBM set.'),
    'base' => 'itkbm_meter_rules_action_itkbm_images_add',
    'named parameter' => FALSE,
    'parameter' => array(
      'images' => array(
        'type' => 'list<file>',
        'label' => t('Images'),
        'description' => t('File entities.'),
        'optional' => FALSE,
        'translatable' => FALSE,
        'default mode' => 'selector',
        'save' => FALSE,
        'wrapped' => FALSE,
      ),
      'depot' => array(
        'type' => 'node',
        'label' => t('ITKBM Depot'),
        'description' => t('@todo'),
        'optional' => FALSE,
        'allow null' => FALSE,
        'translatable' => FALSE,
        'default mode' => 'selector',
        'save' => FALSE,
        'wrapped' => FALSE,
      ),
      'toolkits' => array(
        'type' => 'list<text>',
        'label' => t('Image Toolkits'),
        'description' => t('@todo'),
        'optional' => TRUE,
        'allow null' => TRUE,
        'translatable' => FALSE,
        'default mode' => 'input',
        'default value' => '',
        'options list' => 'itkbm_meter_image_toolkit_options',
      ),
      'image_styles' => array(
        'type' => 'list<text>',
        'label' => t('Image styles'),
        'description' => t('@todo'),
        'optional' => TRUE,
        'allow null' => TRUE,
        'translatable' => FALSE,
        'default mode' => 'input',
        'default value' => '',
        'options list' => 'image_style_options',
      ),
    ),
  );


  $info['itkbm_meter_itkbm_images_remove'] = array(
    'group' => t('Image Toolkit Benchmark'),
    'label' => t('Remove images to an existsing ITKBM set.'),
    'base' => 'itkbm_meter_rules_action_itkbm_images_remove',
    'named parameter' => FALSE,
    'parameter' => array(
      'images' => array(
        'type' => 'list<file>',
        'label' => t('Images'),
        'description' => t('File entities.'),
        'optional' => FALSE,
        'allow null' => FALSE,
        'translatable' => FALSE,
        'default mode' => 'selector',
        'save' => FALSE,
        'wrapped' => FALSE,
      ),
      'depot' => array(
        'type' => 'node',
        'label' => t('ITKBM Depot'),
        'description' => t('@todo'),
        'optional' => FALSE,
        'allow null' => FALSE,
        'translatable' => FALSE,
        'default mode' => 'selector',
        'save' => FALSE,
        'wrapped' => FALSE,
      ),
    ),
  );

  return $info;
}

/**
 * Implements hook_rules_action_ACTION().
 *
 * @param array $images
 * @param object $depot
 * @param array $toolkits
 * @param array $image_styles
 * @param array $settings
 * @param RulesState $rules_state
 * @param RulesAction $rules_action
 * @param string $op
 *   Known values are:
 *   - execute: Do whatever you want.
 *
 * @return array|null
 *   The named provided variables.
 */
function itkbm_meter_rules_action_itkbm_images_add($images, $depot, $toolkits, $image_styles, $settings, $rules_state, $rules_action, $op) {
  switch ($op) {
    case 'execute':
      if (!$toolkits) {
        $toolkits = array_keys(itkbm_meter_image_toolkit_options());
      }

      if (!$image_styles) {
        $image_styles = array_keys(image_style_options(FALSE));
      }

      $standards = itkbm_meter_depot_standardize($depot->nid, $toolkits, $image_styles);
      itkbm_meter_depot_images_add(
        $depot->nid,
        $images,
        $standards['toolkits'],
        $standards['image_styles']
      );
      break;

  }

  return NULL;
}

/**
 * Implements hook_rules_action_ACTION().
 *
 * @param array $images
 * @param object $depot
 * @param array $settings
 * @param RulesState $rules_state
 * @param RulesAction $rules_action
 * @param string $op
 *   Known values are:
 *   - execute: Do whatever you want.
 *
 * @return array|null
 *   The named provided variables.
 */
function itkbm_meter_rules_action_itkbm_images_remove($images, $depot, $settings, $rules_state, $rules_action, $op) {
  switch ($op) {
    case 'execute':
      if (!$depot) {
        return NULL;
      }

      itkbm_meter_depot_images_remove($depot->nid, $images);
      break;
  }

  return NULL;
}
