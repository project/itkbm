<?php
/**
 * @file
 * itkbm_core.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function itkbm_core_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'itkbm_header_general';
  $mini->category = 'Header';
  $mini->admin_title = 'General';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'threecol_25_50_25';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'middle' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '1b0e4fd2-55f8-4413-a5f0-6fd4b8094ecb';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-7bc666ee-e6c9-4411-b309-a3f392422b63';
    $pane->panel = 'left';
    $pane->type = 'page_logo';
    $pane->subtype = 'page_logo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7bc666ee-e6c9-4411-b309-a3f392422b63';
    $display->content['new-7bc666ee-e6c9-4411-b309-a3f392422b63'] = $pane;
    $display->panels['left'][0] = 'new-7bc666ee-e6c9-4411-b309-a3f392422b63';
    $pane = new stdClass();
    $pane->pid = 'new-ab8e1252-91bb-4315-8364-5e2efd8d392f';
    $pane->panel = 'left';
    $pane->type = 'page_site_name';
    $pane->subtype = 'page_site_name';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ab8e1252-91bb-4315-8364-5e2efd8d392f';
    $display->content['new-ab8e1252-91bb-4315-8364-5e2efd8d392f'] = $pane;
    $display->panels['left'][1] = 'new-ab8e1252-91bb-4315-8364-5e2efd8d392f';
    $pane = new stdClass();
    $pane->pid = 'new-5e75685e-c6ce-41e2-a7c1-e51152f29d43';
    $pane->panel = 'middle';
    $pane->type = 'page_primary_links';
    $pane->subtype = 'page_primary_links';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5e75685e-c6ce-41e2-a7c1-e51152f29d43';
    $display->content['new-5e75685e-c6ce-41e2-a7c1-e51152f29d43'] = $pane;
    $display->panels['middle'][0] = 'new-5e75685e-c6ce-41e2-a7c1-e51152f29d43';
    $pane = new stdClass();
    $pane->pid = 'new-ef7cb757-8c4f-4966-af89-b3470dd74c0e';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'user-login';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ef7cb757-8c4f-4966-af89-b3470dd74c0e';
    $display->content['new-ef7cb757-8c4f-4966-af89-b3470dd74c0e'] = $pane;
    $display->panels['right'][0] = 'new-ef7cb757-8c4f-4966-af89-b3470dd74c0e';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-7bc666ee-e6c9-4411-b309-a3f392422b63';
  $mini->display = $display;
  $export['itkbm_header_general'] = $mini;

  return $export;
}
