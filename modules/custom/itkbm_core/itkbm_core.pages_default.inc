<?php
/**
 * @file
 * itkbm_core.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function itkbm_core_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template_panel_context';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Default',
    'no_blocks' => 1,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(),
      'logic' => 'and',
    ),
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
  );
  $display = new panels_display();
  $display->layout = 'radix_moscone';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => FALSE,
        'fixed_width' => '',
        'column_separation' => '0.5em',
        'region_separation' => '0.5em',
        'row_separation' => '0.5em',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'content',
          1 => 'sidebar',
        ),
        'parent' => 'main',
        'class' => 'main-content',
      ),
      'content' => array(
        'type' => 'region',
        'title' => 'Content',
        'width' => '100',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      'sidebar' => array(
        'type' => 'region',
        'title' => 'Sidebar',
        'width' => '200',
        'width_type' => 'px',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'header',
        ),
        'parent' => 'main',
        'class' => 'header',
      ),
      'header' => array(
        'type' => 'region',
        'title' => 'Header',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'sidebar' => NULL,
      'header' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '8d824eac-0f3d-492c-a483-15f19c27d8c0';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-5b40f3b6-3030-4cbc-bba0-d350d06de8c3';
    $pane->panel = 'contentmain';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5b40f3b6-3030-4cbc-bba0-d350d06de8c3';
    $display->content['new-5b40f3b6-3030-4cbc-bba0-d350d06de8c3'] = $pane;
    $display->panels['contentmain'][0] = 'new-5b40f3b6-3030-4cbc-bba0-d350d06de8c3';
    $pane = new stdClass();
    $pane->pid = 'new-c9459e3a-31b4-4763-8ea3-2ef6aad7a7c1';
    $pane->panel = 'contentmain';
    $pane->type = 'pane_messages';
    $pane->subtype = 'pane_messages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = '';
    $pane->uuid = 'c9459e3a-31b4-4763-8ea3-2ef6aad7a7c1';
    $display->content['new-c9459e3a-31b4-4763-8ea3-2ef6aad7a7c1'] = $pane;
    $display->panels['contentmain'][1] = 'new-c9459e3a-31b4-4763-8ea3-2ef6aad7a7c1';
    $pane = new stdClass();
    $pane->pid = 'new-926b16be-0ec7-4bf7-85bb-0958091cf861';
    $pane->panel = 'contentmain';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = '';
    $pane->uuid = '926b16be-0ec7-4bf7-85bb-0958091cf861';
    $display->content['new-926b16be-0ec7-4bf7-85bb-0958091cf861'] = $pane;
    $display->panels['contentmain'][2] = 'new-926b16be-0ec7-4bf7-85bb-0958091cf861';
    $pane = new stdClass();
    $pane->pid = 'new-f0c6c2a7-0a9b-4b08-85fc-e968c6028aae';
    $pane->panel = 'header';
    $pane->type = 'panels_mini';
    $pane->subtype = 'itkbm_header_general';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f0c6c2a7-0a9b-4b08-85fc-e968c6028aae';
    $display->content['new-f0c6c2a7-0a9b-4b08-85fc-e968c6028aae'] = $pane;
    $display->panels['header'][0] = 'new-f0c6c2a7-0a9b-4b08-85fc-e968c6028aae';
    $pane = new stdClass();
    $pane->pid = 'new-b9539216-3b48-451d-aa25-19cfa00da44c';
    $pane->panel = 'sidebar';
    $pane->type = 'block';
    $pane->subtype = 'system-navigation';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = 'b9539216-3b48-451d-aa25-19cfa00da44c';
    $display->content['new-b9539216-3b48-451d-aa25-19cfa00da44c'] = $pane;
    $display->panels['sidebar'][0] = 'new-b9539216-3b48-451d-aa25-19cfa00da44c';
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = 'new-926b16be-0ec7-4bf7-85bb-0958091cf861';
  $handler->conf['display'] = $display;
  $export['site_template_panel_context'] = $handler;

  return $export;
}
