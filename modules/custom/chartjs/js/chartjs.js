/**
 * @file
 * Documentation missing.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.chartjs = {
    attach: function (context, settings) {
      var chartId;
      if (!settings.hasOwnProperty('chartjs') || !settings.chartjs.hasOwnProperty('chart')) {
        return;
      }

      for (chartId in settings.chartjs.chart) {
        if (settings.chartjs.chart.hasOwnProperty(chartId)) {
          $('#' + chartId).once('chartjs', Drupal.chartjs.initialize);
        }
      }
    }
  };

  Drupal.chartjs = Drupal.chartjs || {};
  Drupal.chartjs.chart = Drupal.chartjs.chart || {};

  Drupal.chartjs.initialize = function () {
    var
      chartId = this.id,
      ctx = this.getContext('2d');

    Drupal.chartjs.chart[chartId] = new Chart(ctx).Bar(
      Drupal.settings.chartjs.chart[chartId].data,
      Drupal.settings.chartjs.chart[chartId].options
    );
  };
}(jQuery));
