<?php

/**
 * @file
 * ${FILE_DESCRIPTION}
 */

/**
 * Implements hook_drush_command().
 */
function itkbm_drush_command() {
  $commands = array();

  $commands['itkbm-import-images'] = array(
    'aliases' => array('itkbm-ii'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'description' => dt('Import images.'),
    'required-arguments' => TRUE,
    'arguments' => array(
      'src_dir' => dt('Source directory'),
      'dst_dir' => dt('Destination directory'),
    ),
    'options' => array(
      'extensions' => array(
        'description' => dt('Image extensions to import.'),
        'example-value' => 'jpeg,jpg,png',
        'required' => FALSE,
        'hidden' => FALSE,
      ),
      'recurse' => array(
        'description' => dt('Scan the source directory recursively.'),
        'example-value' => '1',
        'required' => FALSE,
        'hidden' => FALSE,
      ),
    ),
    'examples' => array(
      'drush itkbm-import-images ~/images/foo itkbm/package-01' => dt('@todo.'),
    ),
  );

  return $commands;
}

/**
 * Implements drush_COMMAND_validate().
 *
 * @todo Better error messages.
 */
function drush_itkbm_import_images_validate($src_dir = NULL, $dst_dir = NULL) {
  if (!is_dir($src_dir)) {
    drush_set_error(
      'itkbm_directory_not_exists',
      dt('Directory is not exists: @dir', array('@dir' => $src_dir))
    );

    return;
  }

  if (!is_executable($src_dir)) {
    drush_set_error(
      'itkbm_directory_not_executable',
      dt('Directory is not executable: @dir', array('@dir' => $src_dir))
    );

    return;
  }

  if (!$dst_dir) {
    drush_set_error(
      'itkbm_directory_name_invalid',
      dt('Directory name is invalid: @dir', array('@dir' => $dst_dir))
    );
  }

  $extensions = drush_get_option_list('extensions', array('jpeg', 'jpg', 'png'));
  if ($extensions != array_filter($extensions)) {
    drush_set_error(
      'itkbm_empty_extension',
      dt('Empty extension.')
    );
  }
}

/**
 * Implements drush_COMMAND().
 *
 * @todo Move the logic into module.
 * @todo Batch process.
 */
function drush_itkbm_import_images($src_dir, $dst_dir) {
  $extensions = drush_get_option_list('extensions', array('jpeg', 'jpg', 'png'));
  $extensions_escape = array();
  foreach ($extensions as $extension) {
    $extensions_escape[] = preg_quote($extension, '/');
  }

  $dst_dir = "public://$dst_dir";
  file_prepare_directory($dst_dir, FILE_CREATE_DIRECTORY);

  $file_scan_options = array(
    'recurse' => (bool) drush_get_option('recurse', TRUE),
  );

  $pattern = '/\.' . implode('|', $extensions_escape) . '$/i';
  $prefix_length = drupal_strlen($src_dir) + 1;
  foreach (file_scan_directory($src_dir, $pattern, $file_scan_options) as $image) {
    $relative = drupal_substr($image->uri, $prefix_length);

    $dst_uri = "$dst_dir/$relative";
    if (file_exists($dst_uri) || file_load_multiple(FALSE, array('uri' => $dst_uri))) {
      continue;
    }

    $image->uid = 1;
    $image->filename = pathinfo($image->uri, PATHINFO_BASENAME);
    $image->mime = file_get_mimetype($image->uri);
    $image->status = FILE_STATUS_PERMANENT;
    $image->timestamp = REQUEST_TIME;

    $file = file_copy($image, $dst_uri);
    file_save($file);
    drush_print($file->uri);
  }
}
